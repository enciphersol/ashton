import { Component, OnInit } from '@angular/core';

declare  var jQuery:  any;
declare  var $:  any;

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    jQuery("#videos").click(function () {
        jQuery(this).toggleClass('active');
        jQuery('#video').prop("muted", !jQuery('#video').prop("muted"));
  
    });
  }

}
