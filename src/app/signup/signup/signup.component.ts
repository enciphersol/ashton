import '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { NgForm,FormsModule } from '@angular/forms';
declare const ReWebSDK: any;
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
	registerRequest: any= {};
	 registerError: any;
  registerSuccess: any;
  registersocError: any;
  registersocSuccess: any;
  userDetails: any = {};

  constructor(
   public authService: AuthService,
   ) { }

  ngOnInit(): void {
  }

  doSignup(form: NgForm){
    this.authService.registerSuccess = null
    if (form.valid) {
      console.log(this.registerRequest.email);
      let request:any = {
       
      };
      request = Object.assign({}, this.registerRequest);
      //console.log(request);
      this.authService.register(request).then(
        (res) => {
          
            
        
        }).catch((err) => {
          this.registerError = err;
        });
    }
  }

}
