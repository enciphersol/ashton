import { Component, OnInit } from '@angular/core';
declare  var jQuery:  any;

@Component({
  selector: 'app-myaddress',
  templateUrl: './myaddress.component.html',
  styleUrls: ['./myaddress.component.scss']
})
export class MyaddressComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    jQuery('.popup-login').magnificPopup({
    type: 'inline',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false,
    callbacks: {
      open: function() {
        
      },
      close: function() {
        //console.log("close");
      }
      // e.t.c.
    }
  });

  }

}
