import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyaddressRoutingModule } from './myaddress-routing.module';
import { MyaddressComponent } from './myaddress/myaddress.component';


@NgModule({
  declarations: [MyaddressComponent],
  imports: [
    CommonModule,
    MyaddressRoutingModule
  ]
})
export class MyaddressModule { }
