import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyaddressComponent } from './myaddress/myaddress.component';

const routes: Routes = [{path: "myaddress", component: MyaddressComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyaddressRoutingModule { }
