import { Component, OnInit } from '@angular/core';
import { PlayerService } from 'src/app/shared/services/player.service';
import { NewsService } from 'src/app/shared/services/news.service';
import { VideoService } from 'src/app/shared/services/video.service';

declare  var jQuery:  any;
declare  var $:  any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	
	playercategoryList: any = "";
	playerList: any = "";
	videoList: any = "";
	newsList: any = "";

  slideliveConfig = {
		  //dots: true,
		  infinite: true,
		  arrows:true,
		  slidesToShow:2,
		  autoplay:false,
		  speed: 500,
		responsive: [{
		       
		      breakpoint: 1024,  
		      settings: {
		        slidesToShow: 2
		      }
		      },  
		      {
		      breakpoint: 992,  
		      settings: {
		        slidesToShow: 2
		      }
		      },
		      {
		      breakpoint: 767,  
		      settings: {
		        slidesToShow: 1
		      }

		    }]
		}; 
	slideplayerConfig = {
		  //dots: true,
		  infinite: true,
		  arrows:true,
		  slidesToShow:3,
		  autoplay:false,
		  speed: 500,
		responsive: [{
		       
		      breakpoint: 1024,  
		      settings: {
		        slidesToShow: 3
		      }
		      },  
		      {
		      breakpoint: 992,  
		      settings: {
		        slidesToShow: 3
		      }
		      },
		      {
		      breakpoint: 767,  
		      settings: {
		        slidesToShow: 1
		      }

		    }]
		};
		slidesponsorConfig = {
		  //dots:false,
		  infinite: true,
		  arrows:false,
		  slidesToShow:5,
		  autoplay:true,
		  speed: 500,
		responsive: [{
		       
		      breakpoint: 1024,  
		      settings: {
		        slidesToShow: 5
		      }
		      },  
		      {
		      breakpoint: 992,  
		      settings: {
		        slidesToShow: 2
		      }
		      },
		      {
		      breakpoint: 767,  
		      settings: {
		        slidesToShow: 2
		      }

		    }]
		};

  constructor(
  private playerService: PlayerService,
  private newsService: NewsService,
  private videoService: VideoService
  ) { }

  ngOnInit(): void {
     	this.playerService.getPlayerCategory().then((data) => {
	      this.playercategoryList = data;
	      console.log(this.playercategoryList);
	      setTimeout(function(){
            jQuery(document).ready(function(){
		      jQuery(".courses-name").click(function(){
		        if(jQuery(this).hasClass('active')){
		          // do nothing
		        }else{
		          var tabid = jQuery(this).attr('id');
		          jQuery(".courses-name").removeClass('active');
		          jQuery(this).addClass('active');
		          jQuery(".course-main-content").removeClass('active');
		          jQuery("#"+tabid+"-content").addClass("active");;
		        }
		        return false;
		      });
		    });
            
          },1000);
	    });
  		this.playerService.getPlayer().then((data) => {
	      this.playerList = data;
	      //console.log(this.playerList);
	    });
		this.newsService.getNews().then((data) => {
	      this.newsList = data;
	      //console.log(this.newsList);
	    });
	    this.videoService.getVideo().then((data) => {
	      this.videoList = data;
        	console.log(this.videoList);
	        window.setTimeout(function(){
	          jQuery('.popup-youtube').magnificPopup({
	            type: 'iframe',
	            mainClass: 'mfp-fade',
	            removalDelay: 160,
	            preloader: false,
	        
	            fixedContentPos: false
	          });
	        },1000);
	    });
  }

}
