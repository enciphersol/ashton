import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsService } from 'src/app/shared/services/news.service';

@Component({
  selector: 'app-news-single',
  templateUrl: './news-single.component.html',
  styleUrls: ['./news-single.component.scss']
})
export class NewsSingleComponent implements OnInit {
	newsSingle: any = {};
	newsList: any = "";
  	newsTitle: any = "";
  	newsDesc: any = "";
  	newsImage: any = "";
  	newsPrevious: any = "";
  	newsNext: any = "";
  	newsRequest: any = {};

  constructor(

    private activatedRoute:ActivatedRoute,
  	private newsService: NewsService
  ) { }

  ngOnInit(): void {
  	this.newsService.getNews().then((data) => {this.newsList = data });

    this.activatedRoute.params.subscribe((data)=>{
      if(data.slug){
        this.getNews(data.slug);
        //this.newsRequest = data.slug;
      }
    });
  }
  //request = Object.assign({},console.log(this.newsRequest));

  getNews(newsslug) {
    this.newsRequest.slug = newsslug;
    let request = Object.assign({}, this.newsRequest)
    this.newsService.getSinglenews(request).then((data) => {
       this.newsSingle = data.data || {};
       this.newsTitle = data.data.title;
       this.newsDesc = data.data.description;
       this.newsImage = data.data.image;
       this.newsPrevious = data.is_previous;
       this.newsNext = data.is_next;
       //console.log(this.newsSingle);
      
      });
  }

}
