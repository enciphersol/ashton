import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/shared/services/news.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

	newsList: any = "";

  constructor(
  private newsService: NewsService) { }

  ngOnInit(): void {
  	this.newsService.getNews().then((data) => {
	      this.newsList = data;
	      console.log(this.newsList);
	    });

  }

}
