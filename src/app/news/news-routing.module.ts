import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsComponent } from './news/news.component';
import { NewsSingleComponent } from './news-single/news-single.component';

const routes: Routes = [
{path: "news", component: NewsComponent},
{ path: "news-single/:slug", component: NewsSingleComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule { }
