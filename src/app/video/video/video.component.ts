import { Component, OnInit } from '@angular/core';
import { VideoService } from 'src/app/shared/services/video.service';

declare  var jQuery:  any;
declare  var $:  any;

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {
 
 slickInit(e) {
    
  }
 videoList: any = "";  

  constructor(
  private videoService: VideoService
  ) { }

  ngOnInit(): void {
     
    
      jQuery.extend(true, jQuery.magnificPopup.defaults, {  
      iframe: {
          patterns: {
             youtube: {
                index: 'youtube.com/', 
                id: 'v=', 
                src: 'https://www.youtube.com/embed/%id%?autoplay=1' 
            }
          }
      }
  });
     jQuery('.popup-login').magnificPopup({
        type: 'inline',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false,
        callbacks: {
          open: function() {
            
          },
          close: function() {
            //console.log("close");
          }
          // e.t.c.
        }
      });

      jQuery("#videos").click(function () {
        jQuery(this).toggleClass('active');
        jQuery('#video').prop("muted", !jQuery('#video').prop("muted"));
  
      });
	  this.videoService.getVideo().then((data) => {
	      this.videoList = data;
        console.log(this.videoList);
        window.setTimeout(function(){
          jQuery('.popup-youtube').magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
        
            fixedContentPos: false
          });
        },1000);
	    });
  }

}
