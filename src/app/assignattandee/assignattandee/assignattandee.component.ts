import { Component, OnInit } from '@angular/core';
declare  var jQuery:  any;

@Component({
  selector: 'app-assignattandee',
  templateUrl: './assignattandee.component.html',
  styleUrls: ['./assignattandee.component.scss']
})
export class AssignattandeeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
      jQuery('.popup-login').magnificPopup({
	    type: 'inline',
	    mainClass: 'mfp-fade',
	    removalDelay: 160,
	    preloader: false,

	    fixedContentPos: false,
	    callbacks: {
	      open: function() {
	        
	      },
	      close: function() {
	        //console.log("close");
	      }
	      // e.t.c.
	    }
	  });
  }

}
