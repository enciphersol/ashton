import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignattandeeComponent } from './assignattandee.component';

describe('AssignattandeeComponent', () => {
  let component: AssignattandeeComponent;
  let fixture: ComponentFixture<AssignattandeeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignattandeeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignattandeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
