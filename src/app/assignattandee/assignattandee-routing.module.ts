import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AssignattandeeComponent } from './assignattandee/assignattandee.component';

const routes: Routes = [{path: "assignattandee", component: AssignattandeeComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AssignattandeeRoutingModule { }
