import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssignattandeeRoutingModule } from './assignattandee-routing.module';
import { AssignattandeeComponent } from './assignattandee/assignattandee.component';


@NgModule({
  declarations: [AssignattandeeComponent],
  imports: [
    CommonModule,
    AssignattandeeRoutingModule
  ]
})
export class AssignattandeeModule { }
