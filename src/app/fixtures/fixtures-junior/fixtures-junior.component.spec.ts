import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FixturesJuniorComponent } from './fixtures-junior.component';

describe('FixturesJuniorComponent', () => {
  let component: FixturesJuniorComponent;
  let fixture: ComponentFixture<FixturesJuniorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FixturesJuniorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FixturesJuniorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
