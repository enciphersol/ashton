import { Component, OnInit } from '@angular/core';
import { MatchesService } from 'src/app/shared/services/matches.service';

@Component({
  selector: 'app-fixtures-junior',
  templateUrl: './fixtures-junior.component.html',
  styleUrls: ['./fixtures-junior.component.scss']
})
export class FixturesJuniorComponent implements OnInit {

	matchsList: any = [];
	matchsrequest: any = {
	  matche_status: "Upcoming",
      team_type: "Junior"
	  
	};

  constructor(
  private matchsService: MatchesService
  ) { }

  ngOnInit(): void {
  	let request = Object.assign({}, this.matchsrequest)
      	this.matchsService.getMatchesList(request).then((data : any) => { 
      	this.matchsList = data;
      	console.log(this.matchsList);
    });
  }

}
