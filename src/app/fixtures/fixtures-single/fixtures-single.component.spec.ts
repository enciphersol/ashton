import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FixturesSingleComponent } from './fixtures-single.component';

describe('FixturesSingleComponent', () => {
  let component: FixturesSingleComponent;
  let fixture: ComponentFixture<FixturesSingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FixturesSingleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FixturesSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
