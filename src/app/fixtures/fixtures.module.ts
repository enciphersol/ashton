import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FixturesRoutingModule } from './fixtures-routing.module';
import { FixturesComponent } from './fixtures/fixtures.component';
import { FixturesSingleComponent } from './fixtures-single/fixtures-single.component';
import { FixturesWomenComponent } from './fixtures-women/fixtures-women.component';
import { FixturesJuniorComponent } from './fixtures-junior/fixtures-junior.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';


@NgModule({
  declarations: [FixturesComponent, FixturesSingleComponent, FixturesWomenComponent, FixturesJuniorComponent],
  imports: [
    CommonModule,
    FixturesRoutingModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
  ]
})
export class FixturesModule { }
