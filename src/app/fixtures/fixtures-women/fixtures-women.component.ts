import { Component, OnInit } from '@angular/core';
import { MatchesService } from 'src/app/shared/services/matches.service';

@Component({
  selector: 'app-fixtures-women',
  templateUrl: './fixtures-women.component.html',
  styleUrls: ['./fixtures-women.component.scss']
})
export class FixturesWomenComponent implements OnInit {
	
	matchsList: any = [];
	matchsrequest: any = {
	  matche_status: "Upcoming",
      team_type: "Women"
	  
	};

  constructor(
  	private matchsService: MatchesService
  	) { }

  ngOnInit(): void {
  	let request = Object.assign({}, this.matchsrequest)
      	this.matchsService.getMatchesList(request).then((data : any) => { 
      	this.matchsList = data;
      	console.log(this.matchsList);
    });
  }

}
