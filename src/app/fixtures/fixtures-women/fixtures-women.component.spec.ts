import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FixturesWomenComponent } from './fixtures-women.component';

describe('FixturesWomenComponent', () => {
  let component: FixturesWomenComponent;
  let fixture: ComponentFixture<FixturesWomenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FixturesWomenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FixturesWomenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
