import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FixturesComponent } from './fixtures/fixtures.component';
import { FixturesSingleComponent } from './fixtures-single/fixtures-single.component';
import { FixturesWomenComponent } from './fixtures-women/fixtures-women.component';
import { FixturesJuniorComponent } from './fixtures-junior/fixtures-junior.component';

const routes: Routes = [
	{path: "fixtures", component: FixturesComponent},
	{ path: "fixtures-single/:slug", component: FixturesSingleComponent },
	{ path: "fixtures-women", component: FixturesWomenComponent },
	{ path: "fixtures-junior", component: FixturesJuniorComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FixturesRoutingModule { }
