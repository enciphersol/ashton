import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddaddressComponent } from './addaddress/addaddress.component'; 

const routes: Routes = [{path: "addaddress", component: AddaddressComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddaddressRoutingModule { }
