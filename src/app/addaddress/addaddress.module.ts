import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddaddressRoutingModule } from './addaddress-routing.module';
import { AddaddressComponent } from './addaddress/addaddress.component';


@NgModule({
  declarations: [AddaddressComponent],
  imports: [
    CommonModule,
    AddaddressRoutingModule
  ]
})
export class AddaddressModule { }
