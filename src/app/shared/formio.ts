import { Injectable } from '@angular/core';


export interface Validate {
    required: boolean;
    minLength: any;
    maxLength: any;
    pattern: string;
    custom: string;
    customPrivate: boolean;
    json: string;
    customMessage: string;
}


@Injectable({
    providedIn: 'root'
})

export class Formio {
    public formData: any = {};


    mapApiToForm(apiData: any): void {

        this.formData = {
            'components': [{
                'label': 'Columns',
                'title': 'Form',
                'type': 'columns',
                'columns':
                    [
                        {
                            'components': [],
                            'width': 12,
                            'offset': 0,
                            'push': 0,
                            'pull': 0,
                            'type': 'column',
                            'hideOnChildrenHidden': false,
                            'input': true,
                            'key': '',
                            'tableView': true,
                            'label': ''
                        }

                    ]
            }]
        };
        this.formData.title = apiData.title;
        apiData.fields.forEach((element, key) => {

            if (element.type !== 'custom') {
                this.mapApiFields(element, key);

            }
        });

        // tslint:disable-next-line:max-line-length
        this.formData.components.push({ 'type': 'button', 'customClass': '', 'label': apiData.button.text || 'Inquire Now', 'key': 'submit', 'disableOnInvalid': true, 'theme': 'primary', 'input': true, 'tableView': true });
        return this.formData;
    }


    mapApiFields(elem, key) {
        const fobj = { type: '', key: elem.id, customClass: '', content: '', placeholder: '', label: '', input: true, validate: {} };
        fobj.type = this.updateJsonType(elem);

        // fobj.className = this.updateJsonClassName(elem);
        fobj.customClass = this.updateJsonClassName(elem);


        if (elem.type === 'html') {
            fobj.content = this.updateJsonContent(elem);
        }

        fobj.placeholder = this.updateJsonPlaceholder(elem);
        fobj.label = this.updateJsonLable(elem);

        if (elem.isRequired) {
            fobj.validate = this.updateRequired(elem);
        }
        if (elem.pageNumber) {

            const index = elem.pageNumber - 1;
            if (this.formData.components[0].columns[index] === undefined) {
                this.formData.components[0].columns.push({
                    'components': [],
                    'width': 6,
                    'offset': 0,
                    'push': 0,
                    'pull': 0,
                    'type': 'column',
                    'hideOnChildrenHidden': false,
                    'input': true,
                    'key': '',
                    'tableView': true,
                    'label': ''
                });

                this.formData.components[0].columns[index - 1].width = 6;
            } else {
            }
            this.formData.components[0].columns[index].components.push(fobj);
        } else {
            this.formData.components.push(fobj);
        }
    }

    updateJsonType(elem) {
        switch (elem.type) {
            case 'text':
                return 'textfield';

            case 'html':
                return 'htmlelement';

            case 'textarea':
                return 'textarea';

            case 'phone':
                return 'phoneNumber';

            case 'email':
                return 'email';

            default:
                break;
        }
    }

    updateJsonClassName(elem) {
        return elem.cssClass;
    }

    updateJsonValue(elem) {
        return elem.value;
    }

    updateJsonContent(elem) {
        return elem.content;
    }

    updateJsonPlaceholder(elem) {
        return elem.placeholder;
    }

    updateJsonLable(elem) {
        return elem.label;
    }

    updateRequired(elem) {
        return { 'required': true, 'customMessage': '', 'json': '' };
    }

}

