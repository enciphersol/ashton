import { Pipe, PipeTransform } from '@angular/core';
@Pipe({ name: 'wrapptag' })
export class WrapPTag implements PipeTransform {
  transform(value: any) {
    if (value !== undefined && value !== null) {
      return '<p>' + value + '</p>';
    }
  }
}
