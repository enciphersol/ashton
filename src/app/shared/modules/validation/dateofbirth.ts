import { IValidation, Validation } from './validation';

export class Dateofbirth extends Validation implements IValidation {
    attrs
    isValid(...attrs) {
        this.attrs = attrs;

        return this.value.length > 0 && !(/[0-9]{2}[/][0-9]{2}[/][0-9]{4}/.test(this.value)) 
    }

    getMessage(): string {
        return `Please enter valid birth date`;
    }
}
