import { IValidation, Validation } from './validation';

export class MinNumber extends Validation implements IValidation {
    attrs
    isValid(...attrs) {
        this.attrs = attrs;
        return this.value >= parseInt(this.attrs[0]);
    }

    getMessage(): string {
        return `Please enter more than {attr_0} value`;
    }
}
