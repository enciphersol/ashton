import { IValidation, Validation } from './validation';

export class Alpha extends Validation implements IValidation {
    attrs
    isValid(...attrs) {
        this.attrs = attrs;

        return this.value.length > 0 && !(/[^a-zA-Z\-]/.test(this.value)) 
    }

    getMessage(): string {
        return `Please enter valid name`;
    }
}
