import { Domain } from './domain';
import { IValidation, Validation } from './validation';

export class Email extends Validation implements IValidation {

    isValid() {
        let emailParts: string[] = this.value.toLowerCase().split('@'),
            localPart = emailParts[0],
            domain = emailParts[1];

        if (localPart && domain) {

            if (localPart.indexOf('"') === 0) {
                var len = localPart.length;
                localPart = localPart.replace(/\"/g, '');
                if (localPart.length !== (len - 2)) {
                    return false; // It was not allowed to have more than two apostrophes
                }
            }

            return (new Domain({ ...this, value: domain })) &&
                localPart.indexOf('.') !== 0 &&
                localPart.substring(localPart.length - 1, localPart.length) !== '.' &&
                localPart.indexOf('..') === -1 &&
                !(/[^\w\+\.\-\#\-\_\~\!\$\&\'\(\)\*\+\,\;\=\:]/.test(localPart));
        }

        return false;
    }

    getMessage(): string {
        return `Please enter a valid email address`;
    }
}