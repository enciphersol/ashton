import { Directive, ElementRef, Optional, Host, SkipSelf, Output, EventEmitter, HostListener, Input, Renderer2, ViewContainerRef } from '@angular/core';
import { NgModel } from '@angular/forms';

@Directive({
  selector: '[appFieldInput]',
  host : {
    '[class.is-invalid]': "!isValid"
  }
})
export class FieldInputDirective {


  ngModelChange: EventEmitter<any> = new EventEmitter();
  changeFunc: Function = ($event) => {
    if ($event && $event.target){
      return $event.target.value;
    }else{
      return "";
    }
  };
  
  private _onChangeEvent: string | Function;
  @Input()
  public set onChangeEvent(value: string | Function) {
  
    if (typeof value == 'string'){
      
      this.el.nativeElement.addEventListener(value, this.onInputChange);
    }else{
      this.changeFunc = value
    }
  }
  public get onChangeEvent(): string | Function {
    return this._onChangeEvent;
  }

  constructor(
    public el: ElementRef,
    @Optional() @Host() @SkipSelf()
    public ngModel: NgModel
  ) {
  }


  ngOnDestroy() {
    // if(this.onChangeEvent){
    //   this.el.nativeElement.removeEventListener(this.onChangeEvent, this.onInputChange);
    // }
  }


  private _value: any;

  public get value(): any {
    return this._value;
  }
  
  public set value(value: any) {
    switch (this.el.nativeElement.type) {
      case 'radio':
      case 'checkbox':
        this._value = value;
        if (this.el.nativeElement.value == value) {
          this.el.nativeElement.checked = true;
        } else {
          this.el.nativeElement.checked = false;
        }
        break;
      default:
        this._value = value;
        this.el.nativeElement.value = value;
        break;
    }
  }

  private _isValid: boolean = true;
  public get isValid(): boolean {
    return this._isValid;
  }

  public set isValid(value: boolean) {
    this._isValid = value;
  }

  @HostListener('input', ['$event'])
  @HostListener('change', ['$event'])
  onInputChange($event) {
    let value = this.changeFunc($event);
    this.ngModelChange.emit(value)
  }
}
