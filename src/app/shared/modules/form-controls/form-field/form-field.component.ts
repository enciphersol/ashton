import { Component, OnInit, ElementRef, Input, ContentChildren, QueryList, forwardRef, OnDestroy, Optional, Host, SkipSelf } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, Validator, NG_VALIDATORS, NgModel, ControlContainer, NgForm, NgControl, FormControl } from '@angular/forms';
import { FieldInputDirective } from '../field-input.directive';
import * as Validations from '../../validation';
import { IValidation } from '../../validation/validation';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-form-field',
    templateUrl: './form-field.component.html',
    styleUrls: ['./form-field.component.scss'],
    providers: [
      {
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => FormFieldComponent),
          multi: true
      },
      {
          provide: NG_VALIDATORS,
          useExisting: forwardRef(() => FormFieldComponent),
          multi: true,
      }
    ]
})
export class FormFieldComponent implements OnInit, OnDestroy, ControlValueAccessor, Validator {

  errorMessage: any = [];
  subscriptions: Subscription[] = [];
  parseError: any = false;
  validations_array: any = [];
  errors: any = {};
  control: ElementRef;
  controls: ElementRef[];
  model: NgModel;

  @Input('validation')
  @ContentChildren(FieldInputDirective) field: QueryList<FieldInputDirective>;

  private _validation: string = "";
  private _name: any;
  private _touched: boolean = false;
  private _isValid: boolean = false;
  translate_attrs: any;

  constructor(
    @Optional() @Host() @SkipSelf()
    public ngForm: NgForm,
    private translate:TranslateService
  ) {

    ngForm.ngSubmit.subscribe(()=>{
      this.touched = false;
      this.validate()
    })
  }

  onChange: any = () => { }
  onTouch: any = () => { }
  onValidatorChange: any = () => { }


  @Input()
  public get isValid(): boolean {
    return this._isValid;
  }
  public set isValid(value: boolean) {
    this._isValid = value;
  }

  @Input()
  public get touched(): boolean {
    return this._touched;
  }
  public set touched(value: boolean) {
    this._touched = value;
  }

  @Input()
  public get validation(): string {
    return this._validation;
  }
  public set validation(value: string) {
    this._validation = value;

    this.validations_array = value.split('|').map((item) => {
      let rule_parts: string[] = item.split(':');
      let params = [];
      let name = rule_parts[0];

      if (rule_parts.length > 1) {
        params = rule_parts[1].split(',').map((item) => {
          return item.trim();
        });
      }

      return { name, params }
    });
  }

  @Input('name')
  public get name(): any {
    return this._name || "This field";
  }
  public set name(value: any) {
    this._name = value;
  }

  _value = ""
  get value() {
    return this._value;
  }
  set value(val) {
    this._value = val
    this.onChange(val);
  }


  writeValue(value: any) {
      this.value = value;
      this.field.forEach(field => {
        field.value =  value ? value : null;
      });
  }

  registerOnChange(fn: any) {
    this.onChange = fn
  }

  registerOnTouched(fn: any) {
    this.onTouch = fn
  }

  registerOnValidatorChange(fn: any) {
    this.onValidatorChange = fn
  }

  validate() {
    let field_is_valid: boolean = true;
    this.errorMessage = [];

    if (this.field.length > 0) {

      for (let index = 0; index < this.validations_array.length; index++) {

        const element = this.validations_array[index];
        const validation_name = this.camelCase(element.name);

     //   console.log(validation_name, element.name);

        if (Validations[validation_name]) {

          const obj: IValidation = new Validations[validation_name]({
            $el: this.field.last.el,
            ngForm: this.ngForm,
            ngModel: this
          })

          const is_valid = obj.isValid(...element.params);
          this.validations_array[index].is_valid = is_valid;

          if (!is_valid) {
            field_is_valid = is_valid;
            
            var _attrs = {};
            if (obj.attrs){
              obj.attrs.forEach((element, index) => {
                _attrs["attr_" + index] = element
              });
            }

            //this.translate.get(obj.name.trim()).subscribe((name)=>{
              //
              //var name = this.translate.instant(obj.name.trim());
              
              this.translate_attrs = {
                ...obj,
                name: name,
                ... _attrs
              }
              
              let message = this.translate.instant(obj.getMessage(), this.translate_attrs);
              //let message = obj.getMessage();

              this.errors[validation_name] = message

              this.errorMessage.push(message);

            //})

            //console.log('translate', obj.name, this.translate.get(obj.name.trim()));
          
            
            if (obj.stop) {
              break;
            }
          }
        }
      }

      if (!field_is_valid) {
        this.isValid = false;
      } else {
        this.errors = {};
        this.isValid = true;
      }

      

      let resp = this.isValid ? null : this.errors;

      return resp;

    }
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.subscriptions) {
      for (let index = 0; index < this.subscriptions.length; index++) {
        this.subscriptions[index].unsubscribe();
      }
    }
  }

  ngAfterViewInit() {
    if (this.field.last) {
      this.field.forEach((item) => {
        this.subscriptions.push(item.ngModelChange.subscribe((value) => {
          this.value = value;
          this.touched = item.ngModel.touched;
        }));
      })
    }
  }

  // onControlChange($event: any) {
  //   this.value = $event.target.value
  // }

  camelCase(str) {
    return str.match(/[a-z]+/gi)
      .map(function (word) {
        return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase()
      })
      .join('')
  }

}