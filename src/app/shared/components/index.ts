import { ImageTagComponent } from './image-tag/image-tag.component';
import { LoadingComponent } from './loading/loading.component';
import { ControlMessages } from './control-messages';
import { HttpMessage } from './http-message';

const Components = [
    ImageTagComponent,
    LoadingComponent,
    ControlMessages,
    HttpMessage,
];

export { Components };
