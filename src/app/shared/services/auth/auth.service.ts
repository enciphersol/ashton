import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { apiUrl, defaultToken, toFormData } from '../../utils';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
dataStore: {
        token: string
        user: any
        isLoggedIn: boolean,
        rentLocation: string
    }
registerSuccess:any = null;
  constructor(
  	private http: HttpClient 
  ) { this.dataStore = {
            token: null,
            rentLocation: null,
            user: null,
            isLoggedIn: false
        }
        //this._token = new BehaviorSubject(this.dataStore.token);
        //this._user = new BehaviorSubject(this.dataStore.user);
        //this._rentLocation = new BehaviorSubject(this.dataStore.rentLocation);
        //this._isLoggedIn = new BehaviorSubject(this.dataStore.isLoggedIn);
        }

  register(request: any) {
        return this.http.post(apiUrl('beforeauth/usersignup'), toFormData({ Appuser: request }), {
            headers: {
                ...defaultToken()
            }
        })
            .toPromise()
            
    }
}
