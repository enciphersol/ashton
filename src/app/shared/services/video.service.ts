import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrl, toFormData } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  constructor(
  private http: HttpClient,
  ) { }

  getVideo(request?:any) {
		return this.http.post(apiUrl('beforeauth/getvideolist'),toFormData(request)).toPromise().then((data:any)=>{
     
        return data;
     
    })
  }
}
