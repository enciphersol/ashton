import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrl, toFormData } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(
  private http: HttpClient,) { }

  getNews(request?:any) {
		return this.http.post(apiUrl('beforeauth/getnewslist'),toFormData(request)).toPromise().then((data:any)=>{
     
        return data;
     
    })
  }
  getSinglenews(request: any) {
    return this.http.post(apiUrl('beforeauth/getnewsdetails'),toFormData({Newsletter: request})).toPromise().then((data:any)=>{
        return data;
    })
  }
}
