import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrl, toFormData } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class MatchesService {

  constructor(
  	private http: HttpClient,
  ) { }

  getMatchesList(request?:any) {
    return this.http.post(apiUrl('beforeauth/getmatcheslist'),toFormData({Matches: request})).toPromise().then((data:any)=>{
     
        return data;
     
    })
  }

}
