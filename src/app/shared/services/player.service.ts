import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrl, toFormData } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(
  private http: HttpClient,
  ) { }

 getPlayerCategory(request?:any) {
		return this.http.post(apiUrl('beforeauth/getplayercategory'),toFormData(request)).toPromise().then((data:any)=>{
     
        return data;
     
    })
  }
  getPlayer(request?:any) {
		return this.http.post(apiUrl('beforeauth/getplayerlist'),toFormData(request)).toPromise().then((data:any)=>{
     
        return data;
     
    })
  }

  getSingleplayer(request: any) {
    return this.http.post(apiUrl('beforeauth/getplayerdetails'),toFormData({Players: request})).toPromise().then((data:any)=>{
        return data;
    })
  }
}
