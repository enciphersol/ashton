import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Moduels as CustomModules } from './modules';
import { TranslateModule } from '@ngx-translate/core';
import { Components } from './components';
import { Pipes } from './pipes';


const Modules = [
  FormsModule,
  ReactiveFormsModule,
  ...CustomModules,
  TranslateModule

];

@NgModule({
  declarations: [
    ...Components,
    ...Pipes
  ],
  imports: [
    CommonModule,
    ...Modules,
    HttpClientModule
    
  ],
  providers:[
   
  ],
  exports: [
    ...Modules,
    ...Components,
    ...Pipes
  ]
})
export class SharedModule { }
