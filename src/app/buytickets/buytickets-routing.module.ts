import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuyticketsComponent } from './buytickets/buytickets.component';
import { BuyticketsSingleComponent } from './buytickets-single/buytickets-single.component';

const routes: Routes = [{path: "buytickets", component: BuyticketsComponent},
 {path: "buytickets-single/:slug", component: BuyticketsSingleComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuyticketsRoutingModule { }
