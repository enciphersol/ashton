import { Component, OnInit } from '@angular/core';

declare  var jQuery:  any;
declare  var $:  any;

@Component({
  selector: 'app-buytickets',
  templateUrl: './buytickets.component.html',
  styleUrls: ['./buytickets.component.scss']
})
export class BuyticketsComponent implements OnInit {

  slidesponsorConfig = {
		  //dots:false,
		  infinite: true,
		  arrows:false,
		  slidesToShow:5,
		  autoplay:true,
		  speed: 500,
		responsive: [{
		       
		      breakpoint: 1024,  
		      settings: {
		        slidesToShow: 5
		      }
		      },  
		      {
		      breakpoint: 992,  
		      settings: {
		        slidesToShow: 2
		      }
		      },
		      {
		      breakpoint: 767,  
		      settings: {
		        slidesToShow: 2
		      }

		    }]
		};

  constructor() { }

  ngOnInit(): void {
  }

}
