import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { BuyticketsRoutingModule } from './buytickets-routing.module';
import { BuyticketsComponent } from './buytickets/buytickets.component';
import { BuyticketsSingleComponent } from './buytickets-single/buytickets-single.component';


@NgModule({
  declarations: [BuyticketsComponent, BuyticketsSingleComponent],
  imports: [
    CommonModule,
    BuyticketsRoutingModule,
    SlickCarouselModule
  ]
})
export class BuyticketsModule { }
