import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyticketsSingleComponent } from './buytickets-single.component';

describe('BuyticketsSingleComponent', () => {
  let component: BuyticketsSingleComponent;
  let fixture: ComponentFixture<BuyticketsSingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyticketsSingleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyticketsSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
