import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClubsponsorshipComponent } from './clubsponsorship/clubsponsorship.component';

const routes: Routes = [{path: "clubsponsorship", component: ClubsponsorshipComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClubsponsorshipRoutingModule { }
