import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClubsponsorshipComponent } from './clubsponsorship.component';

describe('ClubsponsorshipComponent', () => {
  let component: ClubsponsorshipComponent;
  let fixture: ComponentFixture<ClubsponsorshipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClubsponsorshipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClubsponsorshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
