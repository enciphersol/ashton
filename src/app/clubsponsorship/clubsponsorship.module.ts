import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClubsponsorshipRoutingModule } from './clubsponsorship-routing.module';
import { ClubsponsorshipComponent } from './clubsponsorship/clubsponsorship.component';


@NgModule({
  declarations: [ClubsponsorshipComponent],
  imports: [
    CommonModule,
    ClubsponsorshipRoutingModule
  ]
})
export class ClubsponsorshipModule { }
