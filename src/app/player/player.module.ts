import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlickCarouselModule } from 'ngx-slick-carousel';

import { PlayerRoutingModule } from './player-routing.module';
import { PlayerComponent } from './player/player.component';
import { PlayerSingleComponent } from './player-single/player-single.component';


@NgModule({
  declarations: [PlayerComponent, PlayerSingleComponent],
  imports: [
    CommonModule,
    PlayerRoutingModule,
    SlickCarouselModule
  ]
})
export class PlayerModule { }
