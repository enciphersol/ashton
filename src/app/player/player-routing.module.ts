import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayerComponent } from './player/player.component';
import { PlayerSingleComponent } from './player-single/player-single.component';

const routes: Routes = [
{path: "player", component: PlayerComponent},
{ path: "player-single/:slug", component: PlayerSingleComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlayerRoutingModule { }
