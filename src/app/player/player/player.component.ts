import { Component, OnInit } from '@angular/core';
import { PlayerService } from 'src/app/shared/services/player.service';

declare  var jQuery:  any;
declare  var $:  any;

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {

	playercategoryList: any = "";
	playerList: any = "";

   slidesponsorConfig = {
		  //dots:false,
		  infinite: true,
		  arrows:false,
		  slidesToShow:5,
		  autoplay:true,
		  speed: 500,
		responsive: [{
		       
		      breakpoint: 1024,  
		      settings: {
		        slidesToShow: 5
		      }
		      },  
		      {
		      breakpoint: 992,  
		      settings: {
		        slidesToShow: 2
		      }
		      },
		      {
		      breakpoint: 767,  
		      settings: {
		        slidesToShow: 2
		      }

		    }]
		    };

  constructor(
  private playerService: PlayerService
  ) { }

  ngOnInit(): void {

  	this.playerService.getPlayerCategory().then((data) => {
	      this.playercategoryList = data;
	      console.log(this.playercategoryList);
	      setTimeout(function(){
            jQuery(document).ready(function(){
		      jQuery(".courses-name").click(function(){
		        if(jQuery(this).hasClass('active')){
		          // do nothing
		        }else{
		          var tabid = jQuery(this).attr('id');
		          jQuery(".courses-name").removeClass('active');
		          jQuery(this).addClass('active');
		          jQuery(".course-main-content").removeClass('active');
		          jQuery("#"+tabid+"-content").addClass("active");;
		        }
		        return false;
		      });
		    });
            
          },1000);
	    });
  	this.playerService.getPlayer().then((data) => {
	      this.playerList = data;
	      console.log(this.playerList);
	    });
	    
     
  }

}
