import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlayerService } from 'src/app/shared/services/player.service';

@Component({
  selector: 'app-player-single',
  templateUrl: './player-single.component.html',
  styleUrls: ['./player-single.component.scss']
})
export class PlayerSingleComponent implements OnInit {
	playerSingle: any = {};
	playerList: any = "";
  	playerTitle: any = "";
  	playerRole: any = "";
  	playerImage: any = "";
  	newsPrevious: any = "";
  	newsNext: any = "";
  	playerRequest: any = {};

  constructor(
  	private activatedRoute:ActivatedRoute,
  	private playerService: PlayerService
  ) { }

  ngOnInit(): void {
  	this.playerService.getPlayer().then((data) => {this.playerList = data });

    this.activatedRoute.params.subscribe((data)=>{
      if(data.slug){
        this.getPlayer(data.slug);
        //this.newsRequest = data.slug;
      }
    });
  }

  getPlayer(playerslug) {
    this.playerRequest.slug = playerslug;
    let request = Object.assign({}, this.playerRequest)
    this.playerService.getSingleplayer(request).then((data) => {
       this.playerSingle = data.data || {};
       this.playerTitle = data.data.full_name;
       this.playerRole = data.data.role;
       this.playerImage = data.data.image;
       this.newsPrevious = data.is_previous;
       this.newsNext = data.is_next;
       console.log(this.playerSingle);
      
      });
  }
}
