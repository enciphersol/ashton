import { Component, OnInit } from '@angular/core';
import { Router,NavigationEnd,ActivatedRoute } from '@angular/router';
declare  var jQuery:  any;

@Component({
  selector: 'app-comman',
  templateUrl: './comman.component.html',
  styleUrls: ['./comman.component.scss']
})
export class CommanComponent implements OnInit {
  intropage: boolean = false;
  homepage: boolean = false;
  signuppage: boolean = false;
  is_fixturessingle: boolean = false;
  is_newssinglepage: boolean = false;
  is_playersingle: boolean = false;
  constructor(
    private router: Router
  ) { 
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if(event.url=='/'){
          //console.log("cw quiz page");
          this.intropage = true;
        }
        else{
          this.intropage = false;
        }
        if(event.url=='/home'){
          //console.log("cw quiz page");
          this.homepage = true;
        }
        else{
          this.homepage = false;
        }
        if(event.url=='/signup'){
          //console.log("cw quiz page");
          this.signuppage = true;
        }
        else{
          this.signuppage = false;
        }
        if(event.url.includes('/fixtures-single')){
            this.is_fixturessingle = true;
          }else{
            this.is_fixturessingle = false;
          }
        
        if(event.url.includes('/news-single')){
            this.is_newssinglepage = true;
          }else{
            this.is_newssinglepage = false;
          }
        if(event.url.includes('/player-single')){
            this.is_playersingle = true; 
          }else{
            this.is_playersingle = false;
          }
        
      }
    });
    
  }
  

  ngOnInit(): void {
     jQuery(".menu-icon").click(function(){
	  jQuery(".menu-icon").toggleClass("active");
	  jQuery(".header-mobile-menu").toggleClass("active");
	  
	});
	jQuery(".header-mobile-menu").click(function(){
	   jQuery(".header-mobile-menu").toggleClass("active");
	   jQuery(".menu-icon").toggleClass("active");
	});

  }

}
