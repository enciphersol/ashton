import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommanComponent } from './comman/comman.component';



const routes: Routes = [
	{
		path : "",
    	component : CommanComponent,
    	 children : [
    	 	{
        		path : "*",
        		loadChildren: () => import('src/app/intro/intro.module').then(m => m.IntroModule)
			},
    	 	{
        		path : "",
        		loadChildren: () => import('src/app/intro/intro.module').then(m => m.IntroModule)
			},
            {
                path : "",
                loadChildren: () => import('src/app/home/home.module').then(m => m.HomeModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/player/player.module').then(m => m.PlayerModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/news/news.module').then(m => m.NewsModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/video/video.module').then(m => m.VideoModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/fixtures/fixtures.module').then(m => m.FixturesModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/buytickets/buytickets.module').then(m => m.BuyticketsModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/checkout/checkout.module').then(m => m.CheckoutModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/address/address.module').then(m => m.AddressModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/signup/signup.module').then(m => m.SignupModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/payment/payment.module').then(m => m.PaymentModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/myaddress/myaddress.module').then(m => m.MyaddressModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/addaddress/addaddress.module').then(m => m.AddaddressModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/clubsponsorship/clubsponsorship.module').then(m => m.ClubsponsorshipModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/cart/cart.module').then(m => m.CartModule)
            },
            {
                path : "",
                loadChildren: () => import('src/app/assignattandee/assignattandee.module').then(m => m.AssignattandeeModule)
            }
    	 ]
	}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
