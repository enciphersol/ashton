import { Component, OnInit } from '@angular/core';

declare  var jQuery:  any;
declare  var $:  any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  slidepaymentConfig = {
		  dots: true,
		  infinite: true,
		  arrows:false,
		  slidesToShow:1,
		  autoplay:false,
		  speed: 500,
		responsive: [{
		       
		      breakpoint: 1024,  
		      settings: {
		        slidesToShow: 1
		      }
		      },  
		      {
		      breakpoint: 992,  
		      settings: {
		        slidesToShow: 1
		      }
		      },
		      {
		      breakpoint: 767,  
		      settings: {
		        slidesToShow: 1
		      }

		    }]
		}; 

  constructor() { }

  ngOnInit(): void {
  }

}
