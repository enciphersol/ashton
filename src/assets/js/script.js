jQuery( document ).ready(function() {
jQuery(window).scroll(function(){
  if (jQuery(window).scrollTop() >= 80) {
    jQuery('.sticky-header').addClass('fixed');
   }
   else {
    jQuery('.sticky-header').removeClass('fixed');
   }
});

jQuery(window).scroll(function(){
    if (jQuery(window).scrollTop() >=990) {
      jQuery('.sticky-header').addClass('readbox');
     }
     else {
      jQuery('.sticky-header').removeClass('readbox');
     }
  });

	jQuery(".menu-icon").click(function(){
	  jQuery(".menu-icon").toggleClass("active");
	  jQuery(".header-mobile-menu").toggleClass("active");
	  
	});
	jQuery(".header-mobile-menu").click(function(){
	   jQuery(".header-mobile-menu").toggleClass("active");
	   jQuery(".menu-icon").toggleClass("active");
	});
	jQuery('.live-slider').slick({
		  //dots: true,
		  infinite: true,
		  arrows:true,
		  slidesToShow:2,
		  autoplay:false,
		  speed: 500,
		responsive: [{
		       
		      breakpoint: 1024,  
		      settings: {
		        slidesToShow: 2
		      }
		      },  
		      {
		      breakpoint: 992,  
		      settings: {
		        slidesToShow: 2
		      }
		      },
		      {
		      breakpoint: 767,  
		      settings: {
		        slidesToShow: 1
		      }

		    }]
		});
});